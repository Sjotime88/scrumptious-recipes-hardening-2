from django.urls import path 
from meal_plans.views import(MealPlanDeleteView)
#TO DO IMPORT VIEW WHEN CREATED 


urlpatterns = [
    path("<int:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplans_delete/")
]