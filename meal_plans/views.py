from django.shortcuts import render, redirect 
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView 
from django.views.generic.list import ListView 

from meal_plans.models import MealPlan 

class MealPlanListView(LoginRequiredMixin, ListView):
    model=MealPlan 
    template_name = "meal_plans/list.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetailview(LoginRequiredMixin, DetailView):
    model=MealPlan
    template_name = 'meal_plans/detail.html'

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = 'meal_plans/new.html'
    fields = []

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plan/detail", pk=plan.id)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal"
    
    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan/detail", args=[self.object.id])

class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plan/delete.html"
    success_url = reverse_lazy("mealplan_list")

    def get_queryset(self): 
        return MealPlan.objects.filter(owner=self.request.user)

